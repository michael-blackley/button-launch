lpTag.events.bind("lpUnifiedWindow","state",function(data) {  //This event triggers for state changes in chat (embedded window only).

console.log("lpUnifiedWindow-state-",data);

})

lpTag.events.bind("lpUnifiedWindow","maximized",function() {  //This event triggers when the chat window is maximized (embedded window only).

console.log("lpUnifiedWindow-maximized");

})

lpTag.events.bind("lpUnifiedWindow","minimized",function() {  //This event triggers when the chat window is minimized (embedded window only).

console.log("lpUnifiedWindow-minimized");

})

lpTag.events.bind("lpUnifiedWindow","windowClosed",function() {  //This event triggers when the chat window is closed (embedded window only).

console.log("lpUnifiedWindow-windowClosed");

})


lpTag.events.bind("LP_OFFERS","OFFER_SHOW",function(data) {  //This signifies the engagement has the potential of being shown on the page.

console.log("LP_OFFERS-OFFER_SHOW-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_DISPLAY",function(data) {  //This signifies the engagement has been rendered and is going to be displayed on the page.

console.log("LP_OFFERS-OFFER_DISPLAY-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_IMPRESSION",function(data) {  //This is triggered when the engagement has been displayed on the page.

console.log("LP_OFFERS-OFFER_IMPRESSION-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_CLICK",function(data) {  //This is when an engagement has been clicked.

console.log("LP_OFFERS-OFFER_CLICK-",data);

//Example of firing this only for invitation:
//if (data.engagementType==='1'){
//your code here
//}




//Example of firing this for a specific engagement (123456789):
//if (data.engagementId==='123456789'){
//your code here
//}

})

lpTag.events.bind("LP_OFFERS","OFFER_DECLINED",function(data) {  //This is when an engagement has been refused. Relevant for invitations only.
//Original event is OFFER_CLOSED. It will be deprecated. OFFER_DECLINED should replace but doesn't work yet. OFFER_REMOVED should trigger when the engagement is removed from thep page (post OFFER_DECLINED and OFFER_TIMEOUT) but won't work either.

console.log("LP_OFFERS-OFFER_DECLINED-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_CLOSED",function(data) {  //This is when an engagement has been refused. Relevant for invitations only.
//Original event is OFFER_CLOSED. It will be deprecated. OFFER_DECLINED should replace but doesn't work yet. OFFER_REMOVED should trigger when the engagement is removed from thep page (post OFFER_DECLINED and OFFER_TIMEOUT) but won't work either.

console.log("LP_OFFERS-OFFER_CLOSED-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_REMOVED",function(data) {  //This is when an engagement has been refused. Relevant for invitations only.
//Original event is OFFER_CLOSED. It will be deprecated. OFFER_DECLINED should replace but doesn't work yet. OFFER_REMOVED should trigger when the engagement is removed from thep page (post OFFER_DECLINED and OFFER_TIMEOUT) but won't work either.

console.log("LP_OFFERS-OFFER_REMOVED-",data);

})

lpTag.events.bind("LP_OFFERS","OFFER_TIMEOUT",function(data) {  //This is when an engagement has been ignored. Relevant for invitations only.

console.log("LP_OFFERS-OFFER_TIMEOUT-",data);

})

/*
For lpUnifiedWindow, data returns the following:
in "state" - init, initialized, uninitialised, prechat, waiting, chatting, interactive, ended, postchat, applicationended. also available - paused, resumepaused, resume, notfound


For LP_OFFERS, data returns the following:
{  
    "contexts": [  
          {"EngagementContext": {"id": "2"}},  
          {"pageContext": {"id": "1738225948"}}  
    ],  
    "campaignId": 248955910,//The campaign Id  
    "engagementId": 248956110,//The engagement Id  
    "engagementRevision": 14,//The revision we are going to show  
    "engagementType": 6,//The engagement type  
    "contextId": "2",//The context (instance in memory on the backend)  
    "zoneId": 471288510,//The zone this is on  
    "state": 2,//The current state of the agents (online, offline) , to set the display state  
    "confKey": "248955910_248956110_14",  
    "tglName": "overlay",//The taglet that was downloaded  
    "done": true,//If the configuration has finished loading  
    "engagementName": "Mine"//The description of the engagement  
} 

state - possible values are :
[
0 - "none",
1 - "online",
2 - "offline"
]

engagementType - possible values are :
[
0 -  "peeling_corner",
1 -  "overlay",
2 -  "toaster",
3 -  "slideout_invite",
5 -  "embedded_button",
6 -  "sticky_button",
]

*/

/*Example of sending data to Google Analytics:
lpTag.events.bind("LP_OFFERS","OFFER_CLICK",function(data) {
    console.log("Data: ",data);
ga('send', 'event', 'OFFER_CLICK', 'LP_OFFERS', data.campaignId);
})*/



